/*
  Moving Animation and Player into their own files.  
*/

//For exit()
#include <stdlib.h>

//For printf
#include <stdio.h>

// For round()
#include <math.h>

#include "SDL2Common.h"
#include "Game.h"
#include "Animation.h"
#include "Player.h"
#include "TextureUtils.h"


/*****************************
 * Global Variables           *
 * ***************************/



const int SDL_OK = 0;



int main(int argc, char* args[]) {
    Game game;
    // SDL allows us to choose which SDL components are going to be// initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);
    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit.
        printf("Error - SDL Initialisation Failed\n");
        exit(1);}initGame(&game);
        runGameLoop(&game);
        cleanUpGame(&game);
        //Shutdown SDL - clear up resources etc.
        SDL_Quit();
        exit(0);


        if(gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          exit(1);
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }
    
     
    // initialise preTimeIndex
    prevTimeIndex = SDL_GetTicks();



        if( SDL_PollEvent( &event ))  // test for events
        { 
            switch(event.type) 
            { 
                case SDL_QUIT:
    		        quit = true;
    		    break;

                // Key pressed event
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    }
                break;

                // Key released event
                case SDL_KEYUP:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        //  Nothing to do here.
                        break;
                    }
                break;
                
                default:
                    // not an error, there's lots we don't handle. 
                    break;    
            }
        }

        // Process Inputs
        processInput(&player, keyStates);

        // Update Game Objects

        updatePlayer(&player, timeDeltaInSeconds);

        //Draw stuff here.

       
    }
    
      





